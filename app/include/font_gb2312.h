#ifndef __FONT_GB2312__
#define __FONT_GB2312__

#define FONT_GB2312_ADDR    0x50000
#define FONT_GB2312_SIZE	32

unsigned char * font_gb2312_get(char * hz);

#endif
