#ifndef __APP_SNTP_H__
#define __APP_SNTP_H__

#define NTP_SERVER1     "ntp.ntsc.ac.cn"
#define NTP_SERVER2     "cn.ntp.org.cn"
#define NTP_INIT_TIME   5       /* 单位：分钟 */
#define NTP_TIMER_DELAY 1000

typedef struct timeDetail{
    unsigned char year;     /* 年2位 */
    unsigned char *month_str;
    unsigned char month;
    unsigned char day;
    unsigned char day_str[3];
    unsigned char hour;
    unsigned char minute;
    unsigned char second;
    unsigned char *week_str;
    unsigned char week;
    unsigned char lmonth_str[8];  /* 农历月份 */
    unsigned char lday_str[6];   /* 农历日 */
}T_TimeDetail;

int8* app_sntp_get_festival(void);
void app_sntp_parse_date(char * date_str, uint8 * year, uint8 * month, uint8 * day);
void app_sntp_parse_time(char * time_str, uint8 * hours, uint8 * minute, uint8 * second);
void app_sntp_init(void);
char * app_sntp_get_time_str(void);
uint8* app_sntp_get_weekstr(void);
T_TimeDetail * app_sntp_get_time(void);

#endif 
