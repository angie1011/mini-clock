#ifndef __APP_WEB_H__
#define __APP_WEB_H__

#define PER_CLOCK_NUM       3
#define CIR_CLOCK_NUM       3
#define CITYP_ID_LEN        10
#define WEB_NOTE_LEN        128


typedef struct timeDetail T_TimeDetail;

typedef enum enable_cfg{
    EN_CLOCK_NOTES = 0,
    EN_WEATHER = 1,
    EN_NEWS = 2,
    EN_SENTENCE = 3,
    EN_MAX = 4
}E_ENABLE_CFG;

typedef struct t_perclock{
    unsigned char enable;       /* �����Ƿ񼤻� */
    unsigned char year;
    unsigned char month;
    unsigned char day;
    unsigned char hour;
    unsigned char minute;
    unsigned char alarm;        /* �Ƿ�������״̬ */
}T_PER_CLOCK;

typedef struct t_circlock{
    unsigned char enable;       /* �����Ƿ񼤻� */
    unsigned char hour;
    unsigned char minute;
    unsigned char week_enable[7];
    unsigned char alarm;        /* �Ƿ�������״̬ */
}T_CIR_CLOCK;

char * app_web_get_cityid(void);
char * app_web_get_note(bool);
void app_web_parse_http(struct espconn * espconn_fd, char * http_data);
void app_web_show_ctlif(struct espconn * espconn_fd);
void app_web_note_set(char * note_info);
void app_web_init(void);
void app_web_data_save(void);
bool app_web_get_data_enable(E_ENABLE_CFG cfg);


/* clock��� */
void app_web_clock_alarm_off(void);
bool app_web_clock_enable_status(void);
bool app_web_clock_alarm_status(void);
void app_web_clock_alarm_on(T_TimeDetail * cur_time);

#endif
