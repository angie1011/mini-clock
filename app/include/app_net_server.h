#ifndef __APP_NET_SERVER_H__
#define __APP_NET_SERVER_H__

#define DNS_PORT    53
#define HTTP_PORT   80

sint8 net_web_server_start(void);
sint8 net_dns_server_start(void);
void net_dns_server_stop(void);
void net_dns_server_restart(void);
void net_web_server_restart(void);

void net_update_info(void);
void net_client_init(unsigned int local_ip);



#endif
