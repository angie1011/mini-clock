#ifndef __DRIVER_OLED12864_H__
#define __DRIVER_OLED12864_H__

#define OLED12864_X_WIDTH                   128                         /* 长 */
#define OLED12864_Y_WIDTH                   64                          /* 宽 */
#define OLED12864_P_WIDTH                   (OLED12864_Y_WIDTH / 8)     /* 页 */

#define OLED12864_I2C_ADDR                  0x78

void oled12864_i2c_init(void);
void oled12864_i2c_clear(void);

/* 下面为显示字符接口 */
void oled12864_i2c_8x6_str(unsigned char x, unsigned char y, char *str, char to_gram);
void oled12864_i2c_8x6_ch(unsigned char x, unsigned char y, char ch, char to_gram);
void oled12864_i2c_8x16_str(unsigned char x, unsigned char y, char *str, char to_gram);
void oled12864_i2c_8x16_ch(unsigned char x, unsigned char y, unsigned char offset, char ch, char to_gram);
void oled12864_i2c_8x16_num(unsigned char x, unsigned char y, unsigned int num_data, char to_gram);
void oled12864_i2c_16x16_hz1(unsigned char x, unsigned char y, unsigned char offset, char * one_hz, char to_gram);
void oled12864_i2c_16x16_hzstr(unsigned char x, unsigned char y, char * hzstr, char to_gram);
bool oled12864_i2c_16x16_roll_hzs(unsigned char x, unsigned char y, unsigned int offset, char *hzstr, char to_gram);
void oled12864_i2c_16x32_ch(unsigned char x, unsigned char y, char, char to_gram);
void oled12864_i2c_16x16_ico(unsigned char x, unsigned char y, unsigned char ico, char to_gram);


/* 下面为画图接口 */
void oled12864_i2c_draw_dot(unsigned char x, unsigned char y, unsigned char d);
void oled12864_i2c_draw_line(unsigned char x0, unsigned char y0,
                                        unsigned char x1, unsigned char y1, unsigned char d);
void oled12864_i2c_draw_triangle(unsigned char x0, unsigned char y0,
                                            unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2, unsigned char d);
void oled12864_i2c_draw_square(unsigned char x0, unsigned char y0,
                                        unsigned char x1, unsigned char y1, unsigned char d);
void oled12864_i2c_draw_square2(unsigned char x0, unsigned char y0,
                                        unsigned char lenght, unsigned char width, bool is_hor, unsigned char d);
void oled12864_i2c_draw_circle(unsigned char x0, unsigned char y0, unsigned char r, unsigned char d);
void oled12864_i2c_draw_refresh(void);
void oled12864_i2c_draw_refresh_pos(unsigned char x0, unsigned char y0, unsigned char x1, unsigned char y1);


#endif
