#ifndef __APP_WIFI_H__
#define __APP_WIFI_H__

#define WIFI_SSID_LEN               32
#define WIFI_PWD_LEN                64
#define WIFI_DATA_FLASH_ADDR        0x3fb000

#define WIFI_STATION_MODE           0x01
#define WIFI_SOFTAP_MODE            0x02

#define WIFI_SSID_FLASH_ADDR        (WIFI_DATA_FLASH_ADDR + 0)
#define WIFI_PASWD_FLASH_ADDR       (WIFI_DATA_FLASH_ADDR + WIFI_SSID_LEN)

#define WIFI_AP_NAME                "MiniClock"

void wifi_init(void);
void wifi_ap_init(void);
bool wifi_station_init(char * wifi_ssid, char * wifi_pwd);
char * wifi_get_station_ip_str(void);
uint32 wifi_get_station_ip(void);

#endif
