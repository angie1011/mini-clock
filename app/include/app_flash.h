#ifndef __APP_FLASH_H__
#define __APP_FLASH_H__

#define FLASH_HEAD_ADDRESS      0x3fa000


void app_flash_write(void * data, uint32 flash_addr, uint32 length);
void app_flash_read(void * data, uint32 flash_addr, uint32 length);


#endif
