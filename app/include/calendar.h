#ifndef  __CALENDAR_H__
#define  __CALENDAR_H__

typedef struct _DateT{
    int year;
    int month;
    int day;
    int reserved;       /* 1表示闰月 */
} T_DateInfo;

extern const char *ChDay[];
extern const char *ChMonth[];

T_DateInfo toSolar(T_DateInfo lunar);   /* 农历转换为阳历 */
T_DateInfo toLunar(T_DateInfo solar);   /* 阳历转换为农历 */
//int CalSolarTerm(unsigned char year, unsigned char month, unsigned char day);
char * FestivalCountdown(unsigned char year, unsigned char month, unsigned char day, int * nday);

#endif
