#include "ets_sys.h"
#include "osapi.h"
#include "driver/uart.h"
#include "user_interface.h"

#include "esp_platform.h"

char esp_id[15] = {0};
uint32 priv_param_start_sec;

static const partition_item_t at_partition_table[] = {
    { SYSTEM_PARTITION_BOOTLOADER,                      0x0,                                                0x1000},
    { SYSTEM_PARTITION_OTA_1,                           0x1000,                                             SYSTEM_PARTITION_OTA_SIZE},
    { SYSTEM_PARTITION_OTA_2,                           SYSTEM_PARTITION_OTA_2_ADDR,                        SYSTEM_PARTITION_OTA_SIZE},
    { SYSTEM_PARTITION_RF_CAL,                          SYSTEM_PARTITION_RF_CAL_ADDR,                       0x1000},
    { SYSTEM_PARTITION_PHY_DATA,                        SYSTEM_PARTITION_PHY_DATA_ADDR,                     0x1000},
    { SYSTEM_PARTITION_SYSTEM_PARAMETER,                SYSTEM_PARTITION_SYSTEM_PARAMETER_ADDR,             0x3000},
    { SYSTEM_PARTITION_CUSTOMER_PRIV_PARAM,             SYSTEM_PARTITION_CUSTOMER_PRIV_PARAM_ADDR,          0x1000},
};

void ICACHE_FLASH_ATTR user_pre_init(void)
{
    if(!system_partition_table_regist(at_partition_table, sizeof(at_partition_table)/sizeof(at_partition_table[0]), SPI_FLASH_SIZE_MAP)) {
        os_printf("system_partition_table_regist fail\r\n");
        while(1);
    }
}

void ICACHE_FLASH_ATTR esp_init(void)
{
    partition_item_t partition_item;
    struct rst_info *rtc_info = system_get_rst_info();
    
    uart_init(UART_BAUD_RATE, UART_BAUD_RATE);
    system_update_cpu_freq(160);
    
    os_printf("=>Last reset reason: %s\n", rtc_info->reason==REASON_WDT_RST || REASON_SOFT_WDT_RST == rtc_info->reason ? "WDT RESET":
                                         (rtc_info->reason==REASON_EXCEPTION_RST ? "EXCEPTION RESET":"other"));
    if (rtc_info->reason == REASON_WDT_RST ||
        rtc_info->reason == REASON_EXCEPTION_RST ||
        rtc_info->reason == REASON_SOFT_WDT_RST) {
        if (rtc_info->reason == REASON_EXCEPTION_RST) {
            os_printf("=>Fatal exception (%d):\n", rtc_info->exccause);
        }
        os_printf("=>epc1=0x%08x, epc2=0x%08x, epc3=0x%08x, excvaddr=0x%08x, depc=0x%08x\n",
                rtc_info->epc1, rtc_info->epc2, rtc_info->epc3, rtc_info->excvaddr, rtc_info->depc);
    }

    os_printf("=>SDK version:%s, MinClock version %d.%d\n", system_get_sdk_version(), MINCLOCK_MAJOR_VERSION, MINCLOCK_MINOR_VERSION);
    if (!system_partition_get_item(SYSTEM_PARTITION_CUSTOMER_PRIV_PARAM, &partition_item)) {
        os_printf("Get partition information fail\n");
    }
    priv_param_start_sec = partition_item.addr/SPI_FLASH_SEC_SIZE;
    os_printf("=>mem info:\n");
    system_print_meminfo();
    system_soft_wdt_feed();
    os_sprintf(esp_id, "%x%x", spi_flash_get_id(), system_get_chip_id());
    os_printf("\n=>esp8266 id is [%s]\n", esp_id);
}

char * ICACHE_FLASH_ATTR esp_get_id(void)
{
    return esp_id;
}

