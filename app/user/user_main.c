#include "ets_sys.h"
#include "osapi.h"
#include "mem.h"
#include "user_interface.h"
#include "driver/i2c_master.h"

#include "esp_platform.h"
#include "app_wifi.h"
#include "driver_oled12864.h"
#include "app_net_server.h"
#include "app_sntp.h"
#include "app_web.h"
#include "driver_sht3x.h"


#define KEY_VAL     GPIO_INPUT_GET(GPIO_ID_PIN(0))
#define CLOCK_ON    GPIO_OUTPUT_SET(GPIO_ID_PIN(12), 0)
#define CLOCK_OFF   GPIO_OUTPUT_SET(GPIO_ID_PIN(12), 1)
#define CLOCK_INIT  PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTDI_U, FUNC_GPIO12);\
                    GPIO_OUTPUT_SET(GPIO_ID_PIN(12), 1)
#define KEY_INIT    PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO0_U, FUNC_GPIO0);\
                            GPIO_DIS_OUTPUT(GPIO_ID_PIN(0))

#define TASK_TIME           200     /* 任务执行间隔 单位：毫秒 */
#define SCREEN_TIME         100     /* 频幕刷新时间   单位：毫秒 */
#define CLOCK_AUTO_STOP     10      /* 闹钟自动停止时间 单位：分钟 */


os_timer_t comm_timer;
os_timer_t sntp_timer;
os_timer_t screen_timer;
os_timer_t roll_screen_timer;

unsigned char weather[256] = "";
unsigned char sentence[256] = "";
unsigned char news[512] = "";
unsigned char g_ipaddr[128] = "";
unsigned char alarm = 0;


void ICACHE_FLASH_ATTR clock_ico_update(void)
{
    if (app_web_clock_enable_status())
    {
        oled12864_i2c_16x16_ico(105, 4, 4, 1);
    }
    else
    {
        oled12864_i2c_16x16_ico(105, 4, 0, 1);
    }
}

void ICACHE_FLASH_ATTR  screen_show_timer(void)
{
    T_TimeDetail *time_info = app_sntp_get_time();
    uint8 * time_str = app_sntp_get_time_str();
    system_soft_wdt_feed();

    if (!time_str)
    {
    	return;
    }

    /* 月 */
    oled12864_i2c_8x16_str(0, 6, app_sntp_get_month(time_str), 1);
    oled12864_i2c_8x16_ch(16, 6, 0, '-', 1);

    /* 日 */
    oled12864_i2c_8x16_ch(24, 6, 0, time_str[8], 1);
    oled12864_i2c_8x16_ch(32, 6, 0, time_str[9], 1);

    /* 周 */
    oled12864_i2c_16x16_hz1(0, 2, 0, "周", 1); // test
    oled12864_i2c_16x16_hz1(0, 4, 0, app_sntp_get_weekstr(), 1);

    /* 时 */
    oled12864_i2c_16x32_ch(16, 2, time_str[11], 1);
    oled12864_i2c_16x32_ch(32, 2, time_str[12], 1);
    
    /* 分 */
    oled12864_i2c_16x32_ch(56, 2, time_str[14], 1);
    oled12864_i2c_16x32_ch(72, 2, time_str[15], 1);

    /* 秒 */
    oled12864_i2c_8x6_ch(49, 3, time_str[17], 1);
    oled12864_i2c_8x6_ch(49, 4, time_str[18], 1);

    /* 农历月 */
    if (os_strlen(time_info->lmonth_str) <= 5)
    {
        oled12864_i2c_16x16_hzstr(48, 6, "  ", 1);  /* 占空位 */
        oled12864_i2c_16x16_hzstr(64, 6, time_info->lmonth_str, 1);
    }
    else
    {
        oled12864_i2c_16x16_hzstr(48, 6, time_info->lmonth_str, 1);
    }

    /* 农历日 */
    oled12864_i2c_16x16_hzstr(96, 6, time_info->lday_str, 1);

    /* 画边界 */
    oled12864_i2c_draw_line(16, 16, 87, 16, 1);
    oled12864_i2c_draw_line(47, 47, 47, 63, 1);
    oled12864_i2c_draw_line(0, 47, 87, 47, 1);
}

void ICACHE_FLASH_ATTR assem_roll_info(char * hzinfo, unsigned char * info)
{
    char * buffer;
    int len = 0;
    
    if (info)
        len = os_strlen(info);

    if (len){
        buffer = os_zalloc(len + 1);
        os_sprintf(buffer, "%s", info);
        os_strcat(hzinfo, buffer);
        os_free(buffer);
    }
}

void ICACHE_FLASH_ATTR screen_show_wifi_dbm(void)
{
    sint8 wifi_dbm = wifi_station_get_rssi();
    if (wifi_dbm < -80)
    {
        oled12864_i2c_16x16_ico(88, 4, 1, 1);
    }
    else if (wifi_dbm < -40)
    {
        oled12864_i2c_16x16_ico(88, 4, 2, 1);
    }
    else if (wifi_dbm < 10)
    {
        oled12864_i2c_16x16_ico(88, 4, 3, 1);
    }
    else
    {
        oled12864_i2c_16x16_ico(88, 4, 0, 1);
    }
}

void ICACHE_FLASH_ATTR screen_show(void * arg)
{
    char hzinfo[1024];
    static unsigned int offset = 0,offset1=0; /* 像素偏移 */
    char * str = wifi_get_station_ip_str();

    /* 组装滚动字幕 */
    os_sprintf(g_ipaddr, "【IP:%s】", str ? str : wifi_get_opmode()==WIFI_STATION_MODE?"连接中...":"请连接以“MiniClock-”开头的无线网，然后浏览器输入192.168.4.1配置无线网密码");
    os_strcpy(hzinfo, g_ipaddr);
    str = app_sntp_get_festival();      /* 节日 */
    if (os_strlen(str)) os_strcat(hzinfo, str);
    if(app_web_get_data_enable(EN_WEATHER))assem_roll_info(hzinfo, weather);    /* 天气 */
    assem_roll_info(hzinfo, app_web_get_note(false));
    if(app_web_get_data_enable(EN_NEWS))assem_roll_info(hzinfo, news);
    if(app_web_get_data_enable(EN_SENTENCE))assem_roll_info(hzinfo, sentence);
    
    
    if(os_strlen(hzinfo)*8 <= offset){
        offset = 0;
    }

    /* 显示滚动字 */
    if(!oled12864_i2c_16x16_roll_hzs(0, 0, offset, hzinfo, 1))
       offset = 0;
    offset++;

    oled12864_i2c_draw_line(0, 15, 127, 15, 1);

    /* 显示wifi强度 */
    screen_show_wifi_dbm();
    
    /* 显示时间 */
    screen_show_timer();

    /* 闹钟滚动屏 */
    if (app_web_clock_alarm_status() && app_web_get_note(true) && app_web_get_data_enable(EN_CLOCK_NOTES))
    {
        os_sprintf(hzinfo,"闹钟提示:%s",app_web_get_note(true));
        if(!oled12864_i2c_16x16_roll_hzs(48, 6, offset1, hzinfo, 1))
            offset1 = 0;
        offset1+=3;
    }

    /* 边界绘制 */
    oled12864_i2c_draw_line(87, 47, 127, 47, 1);
    oled12864_i2c_draw_line(87, 16, 87, 47, 1);
    oled12864_i2c_draw_line(87, 32, 127, 32, 1);

    /* 刷新 */
    oled12864_i2c_draw_refresh();

    system_soft_wdt_feed();
}

/* 200ms响应一次 */
void ICACHE_FLASH_ATTR common_timer(void * arg)
{
    ip_addr_t addr;
    static unsigned int cnt = 0, al=0;
    uint8 state = wifi_get_opmode(), tmp;

    cnt++;
    if (cnt % 2 == 0)  /* 400ms执行一次的任务 */
    {
        /* 喂看门狗 */
        system_soft_wdt_feed();

        /* 闹钟响应 */
        if (app_web_clock_alarm_status())
        {
            /* 闹钟根据一定的频率响 */
            tmp = al++ % 9;
            if (tmp == 0 || tmp == 1 || tmp == 3 || tmp == 5)
                CLOCK_ON;
            else if (tmp == 2 || tmp == 4 || tmp == 6 || tmp == 7 || tmp == 8)
                CLOCK_OFF;

            if (al >= (CLOCK_AUTO_STOP * 60000 / (TASK_TIME * 2))) /* 10min后闹钟自动关闭 */
            {
                al = 0;
                app_web_clock_alarm_off();
                CLOCK_OFF;
            }
        }
    }
    else if (cnt % 5 == 0)  /* 1s一次的check任务 */
    {
        if (state == WIFI_STATION_MODE)
        {
            /* ntp时间更新 */
            app_sntp_init();
        }
        else if (state == WIFI_SOFTAP_MODE)
        { 
            os_printf("wifi opmode: softAP\n");
        }
    }

    if (cnt % 10 == 0)      /* 2s一次的check任务 */
    {
        char buff[8];
        int temp = 0, humi = 0;

        /* 更新温湿度 */
        sht3x_get_temp_humi(&temp, &humi);
        if(temp != 0 && humi != 0)
        {
            os_sprintf(buff, "%d.%d", temp/10, temp%10);
            oled12864_i2c_8x6_str(88, 2, buff, 1);
            os_sprintf(buff, "%d%", humi / 10);
            oled12864_i2c_8x6_str(94, 3, buff, 1);
            oled12864_i2c_16x16_hz1(112, 2, 0, "℃", 1);
        }

        /* 更新闹钟图标 */
        clock_ico_update();
    }

    /* 按键按下 */
    if(KEY_VAL == 0x00)
    {
        os_delay_us(1000);
        if(KEY_VAL == 0x00)
        {
            app_web_clock_alarm_off();
            CLOCK_OFF;
        }
    }
}

void ICACHE_FLASH_ATTR module_init(void)
{
    char show_info[24];


    os_printf("=>esp system init...\n");
    esp_init();                 /* esp板卡相关的初始化 */

    os_printf("=>i2c gpio init...\n");
    i2c_master_gpio_init();     /* i2c引脚初始化 */
    
    os_printf("=>screen init...\n");
    oled12864_i2c_init();       /* 屏幕初始化 */
    oled12864_i2c_16x16_hzstr(0, 0, "系统正在初始化", 0);
    os_delay_us(65000);
    
    os_printf("=>app web init...\n");   
    oled12864_i2c_8x6_str(0, 2, "=>app web init...", 0);
    app_web_init();             /* app 应用数据初始化 */
    os_delay_us(65000);

    os_printf("=>wifi init...\n");
    oled12864_i2c_8x6_str(0, 3, "=>wifi init...", 0);
    wifi_init();	            /* wifi连接 */
    os_delay_us(65000);

    /* 按键的初始化 */
    os_printf("=>gpio init...\n");
    oled12864_i2c_8x6_str(0, 4, "=>gpio init...", 0);
    KEY_INIT;
    CLOCK_INIT;
    PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO2_U, FUNC_GPIO2);    /* 关灯 */
    GPIO_OUTPUT_SET(GPIO_ID_PIN(2), 0);
    os_delay_us(65000);

    os_printf("=>sensor init...\n");
    oled12864_i2c_8x6_str(0, 5, "=>sensor init...", 0);
    sht3x_init();               /* 温湿度传感器初始化 */
    os_delay_us(65000);

    os_sprintf(show_info, "=>clock(v%d.%d) init ok", MINCLOCK_MAJOR_VERSION, MINCLOCK_MINOR_VERSION);
    oled12864_i2c_8x6_str(0, 6, show_info, 0);
    os_sprintf(show_info, "=>[id-%s]", esp_get_id());
    oled12864_i2c_8x6_str(0, 7, show_info, 0);
    os_delay_us(65000);

//    int d = 0;
//    char *fes;
//    fes = FestivalCountdown(23, 5, 28, &d); os_printf("23-5-28:距离%s还剩%d天\n", fes?fes:"nil", d);
}

void ICACHE_FLASH_ATTR user_init(void)
{
    /* 系统初始化 */
    module_init();

    /* 系统初始化完成，刷新一次屏幕 */
    system_soft_wdt_feed();
    os_delay_us(65000);
    os_delay_us(65000);
    os_delay_us(65000);
    oled12864_i2c_16x16_hzstr(0, 0, "系统初始化完成！", 0);
    os_delay_us(65000);
    oled12864_i2c_clear();
    system_soft_wdt_feed();

    /* 系统公共定时器，可定时用于获取某状态 */
    os_timer_disarm(&comm_timer);
    os_timer_setfn(&comm_timer, (ETSTimerFunc *)(common_timer), NULL);
    os_timer_arm(&comm_timer, TASK_TIME, true);

    os_timer_disarm(&screen_timer);
    os_timer_setfn(&screen_timer, (ETSTimerFunc *)(screen_show), NULL);
    os_timer_arm(&screen_timer, SCREEN_TIME, true);
}
