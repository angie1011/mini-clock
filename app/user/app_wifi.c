#include "osapi.h"
#include "ip_addr.h"
#include "user_interface.h"

#include "app_wifi.h"
#include "app_net_server.h"

enum wifi_station_state{
    NOT_CONNECT,
    CONNECTTING,
    CONNECTED,
};

static os_timer_t os_wifi_timer;
static enum wifi_station_state station_state = NOT_CONNECT;
static char wifi_station_ip_str[16] = {0};
static unsigned int wifi_station_ip = 0;

void ICACHE_FLASH_ATTR wifi_state_check(void * arg)
{
    static int conn_cnt = 0;
    uint8 get_conn_state;
    struct ip_info ip = {{0}, {0}, {0}};

    system_soft_wdt_feed();

    get_conn_state = wifi_station_get_connect_status();
    if (get_conn_state == STATION_GOT_IP) {
        wifi_get_ip_info(STATION_IF, &ip); //连接成功后可以查询本模块的IP地址
        if (ip.ip.addr)
        {
            wifi_station_ip = ip.ip.addr;
            os_sprintf(wifi_station_ip_str, IPSTR, IP2STR(&ip.ip.addr));
            os_printf("=>Clock get ip:%s(0x%x)\n", wifi_station_ip_str,wifi_station_ip);

            conn_cnt = 0;
            station_state = CONNECTED;

            wifi_station_set_auto_connect(1);
            net_dns_server_stop();
            net_web_server_restart();
            net_client_init(ip.ip.addr);
            os_timer_disarm(&os_wifi_timer);
            return;
        }
    }
    
    if (++conn_cnt >= 60) /* 1min连接时间 */ 
    {
        os_printf("x>connect wifi failed<%d>\n",get_conn_state);
        os_timer_disarm(&os_wifi_timer);
        wifi_ap_init();
        conn_cnt = 0;
    }
}

void wifi_scan_done_cb(void *arg, STATUS status)
{
    struct bss_info *bss_link = (struct bss_info *)arg;
    while (bss_link != NULL)
    {
        os_printf("scan-ssid:%s >scan-channel:%d >scan-authmode:%d\n\n", bss_link->ssid, bss_link->channel, bss_link->authmode);
        bss_link = bss_link->next.stqe_next;
    }
}

void ICACHE_FLASH_ATTR wifi_timer_start(void)
{
    os_timer_disarm(&os_wifi_timer);
    os_timer_setfn(&os_wifi_timer, (ETSTimerFunc *)(wifi_state_check), NULL);
    os_timer_arm(&os_wifi_timer, 1000, true);
//    wifi_station_scan(NULL, wifi_scan_done_cb);   //扫描当前有哪些WIFI
}

char * ICACHE_FLASH_ATTR wifi_get_station_ip_str(void)
{
    if (station_state == CONNECTED && wifi_station_get_connect_status() == STATION_GOT_IP)
    {
        return wifi_station_ip_str;
    }
    return NULL;
}

uint32 ICACHE_FLASH_ATTR wifi_get_station_ip(void)
{
    if (station_state == CONNECTED && wifi_station_get_connect_status() == STATION_GOT_IP)
    {
        return wifi_station_ip;
    }
    return 0;
}


void ICACHE_FLASH_ATTR wifi_ap_init(void)
{
    bool ret;
    struct ip_info IP_info;
    char self_wifi_ssid[WIFI_SSID_LEN] = {0};
    struct softap_config ap_config;
    uint32 chip_id = system_get_chip_id();

    if (wifi_get_opmode() != WIFI_SOFTAP_MODE)
    {
        wifi_station_disconnect();
    }

    os_sprintf(self_wifi_ssid, "%s-%x",WIFI_AP_NAME,chip_id);
    os_printf("=>create ap %s.\n", self_wifi_ssid);
    os_strcpy(ap_config.ssid, self_wifi_ssid);      // 创建wifi
    ap_config.password[0] = 0;
    ap_config.ssid_len = os_strlen(ap_config.ssid); // 设置wifi的ssid长度
    ap_config.channel = 1;                          // 通道号1～13
    ap_config.authmode = AUTH_OPEN;                 // 设置不加密
    ap_config.ssid_hidden= 0 ;                      // 不隐藏SSID
    ap_config.max_connection= 2 ;                   // 最大连接数
    ap_config.beacon_interval = 100;                // 信标间隔时槽100～60000 ms
    wifi_station_ip_str[0] = '\0';
    station_state = NOT_CONNECT;

    wifi_set_opmode(WIFI_SOFTAP_MODE);              //设置为SoftAP模式
    ret = wifi_softap_set_config(&ap_config);       // 使wifi配置生效，保存配置
    if (!ret){
        os_printf("x>set AP mode failed.\n");
        while(1);   /* 等待重启 */
    }

    net_dns_server_restart(); 
    net_web_server_restart();
}

bool ICACHE_FLASH_ATTR wifi_station_init(char * wifi_ssid, char * wifi_pwd)
{
    struct station_config station_conf;

    if (station_state == CONNECTTING 
        || os_strlen(wifi_ssid) >= WIFI_SSID_LEN 
        || os_strlen(wifi_pwd) >= WIFI_PWD_LEN)
    {
        return false;
    }

    os_printf("=>get ssid:%s, pwd:%s\n", wifi_ssid, wifi_pwd);

    if (station_state == CONNECTED)
    {
        wifi_station_disconnect();
        station_state == CONNECTTING;
    }

    wifi_set_opmode(WIFI_STATION_MODE);                 /* 设置为STATION模式 */
    os_bzero(&station_conf, sizeof(station_conf));
    os_strcpy(station_conf.ssid, wifi_ssid);            /* 无线网名字 */
    os_strcpy(station_conf.password, wifi_pwd);         /* 无线网密码 */
    station_conf.all_channel_scan = true;               
    wifi_station_set_config(&station_conf);             /* 设置WiFi station接口配置，并保存到 flash */
    wifi_station_connect();                             /* 连接路由器 */
    wifi_timer_start();                                 /* 定时器检测连接状态 */

    return true;
}

void ICACHE_FLASH_ATTR wifi_init(void)
{
    uint8 auto_con, wifi_mode;

    wifi_mode = wifi_get_opmode();
    auto_con = wifi_station_get_auto_connect();
    os_printf("=>wifi is %s mode, %s auto connect\n", 
                wifi_mode==WIFI_STATION_MODE?"station":"softAP", auto_con?"is":"not");

    struct	station_config	config[5];
    wifi_station_get_ap_info(config);
    os_printf("=>ssid:%s, pwd:%s\n",config[0].ssid, config[0].password);

    /* 判断是否会自动连接上次保存的wifi信息 */
    if (auto_con && (wifi_mode == WIFI_STATION_MODE))
    {
        station_state = CONNECTTING;
        wifi_timer_start();
    }
    else
    {
        /* 不自动连接则需要配置wifi信息 */
        wifi_ap_init();
    }
    wifi_station_ap_number_set(1);  /* 设置保存1个wifi信息 */
}
