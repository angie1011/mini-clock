#include "mem.h"
#include "osapi.h"
#include "spi_flash.h"
#include "ip_addr.h"
#include "espconn.h"
#include "json/jsonparse.h"
#include "user_interface.h"

#include "app_server_addr.h"

#include "app_net_server.h"
#include "app_web.h"
#include "app_wifi.h"

/*  服务器信息,获取天气，新闻等实时数据的服务器地址
    注意这里因一些数据来源问题需要自行提供自己的服务器地址, 也可使用作者的服务器，如有需要请联系 */
#ifndef SERVER_ADDR
#define SERVER_ADDR_STR "4.3.2.1"
#define SERVER_ADDR     0X01020304
#define SERVER_PORT     80
#endif

struct espconn g_dns_server;
struct espconn g_web_server;
struct espconn g_tcp_client_conn;

extern unsigned char weather[128];
extern unsigned char sentence[128];
extern unsigned char news[512];

void ICACHE_FLASH_ATTR tcp_reconn_cb(void *arg, sint8 err) {
    os_printf("x>Connection failed<%d>, maybe connecte infomation(0x%x) is error\n", err, arg);
    os_delay_us(10000);
}

void ICACHE_FLASH_ATTR user_tcp_recon_cb(void *arg, sint8 err) {
    os_printf("连接错误，错误代码为%d\r\n", err);
    espconn_connect((struct espconn *) arg); //  连接失败可以重连
}

//正常断开TCP连接
void ICACHE_FLASH_ATTR tcp_web_server_disconn_cb(void *arg)
{
    os_printf("=>TCP normal disconnect\n");
}

void ICACHE_FLASH_ATTR tcp_web_server_sent_cb(void *arg)
{
    os_printf("=>TCP seriver send over\n");
}

//作为服务器收到客户端TCP报文的回调函数
void ICACHE_FLASH_ATTR tcp_web_server_received_cb(void * arg, char * pdata, unsigned short len)
{
    char * http_data = os_strstr(pdata, "MinClock-12341");  // TODO: 小时钟的ID信息
    if (http_data)
        app_web_parse_http(arg, http_data);
    else
        app_web_cfg_inf((struct espconn *)arg);

    system_soft_wdt_feed();
    return;
}

void ICACHE_FLASH_ATTR net_dns_recv(void * arg, char * pdata, uint16 len)
{
    struct espconn* ep = arg;
    remot_info * remote_info = NULL;    // 远端连接信息结构体指针
    uint8 buffer[256];

    if (pdata != NULL)
    {
        if( os_strstr((const char *)pdata + 0xc, "taobao")  ||
            os_strstr((const char *)pdata + 0xc, "qq")      || 
            os_strstr((const char *)pdata + 0xc, "sogou")   ||
            os_strstr((const char *)pdata + 0xc, "wexin")   ||
            os_strstr((const char *)pdata + 0xc, "alipay")  ||
            os_strstr((const char *)pdata + 0xc, "360")     ||
            os_strstr((const char *)pdata + 0xc, "iqiyi")   ||
            os_strstr((const char *)pdata + 0xc, "baidu"))
        {
            return;
        }
        if(espconn_get_connection_info(ep, &remote_info, 0) == ESPCONN_OK)      // 获取远端信息
        {
            ep->proto.udp->remote_port  = remote_info->remote_port;             // 获取对方端口号
            os_memcpy(ep->proto.udp->remote_ip, remote_info->remote_ip, 4);     // 获取对方IP地址
        }
        
        /* dns请求回复只需要修改几个中值即可 */
        pdata[2] = 0x81;
        pdata[3] = 0x80;
        pdata[7] = 0x01;
        os_memcpy(buffer, pdata, len > sizeof(buffer) ? sizeof(buffer) : len);
        os_memcpy(buffer + len, "\xC0\x0C\x00\x01\x00\x01\x00\x00\x00\x1e\x00\x04\xc0\xa8\x04\x01", 16); 

        /* 告知对方回复地址为192.168.4.1 */
        espconn_send((struct espconn*)arg, buffer, 16 + len);
    }
}

/* 建立了TCP连接，设置发送、接收数据回调、断开连接回调 */
void ICACHE_FLASH_ATTR tcp_web_server_connected_cb(void *arg)
{
    espconn_regist_sentcb((struct espconn*)(arg),tcp_web_server_sent_cb);           // 注册网络数据发送成功的回调函数
    espconn_regist_recvcb((struct espconn*)(arg),tcp_web_server_received_cb);       // 注册网络数据接收成功的回调函数
    espconn_regist_disconcb((struct espconn*)(arg),tcp_web_server_disconn_cb);      // TCP连接正常，断开连接的回调函数
}

sint8 ICACHE_FLASH_ATTR net_dns_server_start(void)
{
    /* dns服务是udp协议，53端口 */
    g_dns_server.type = ESPCONN_UDP;
    g_dns_server.proto.udp = (esp_udp *)os_zalloc(sizeof(esp_udp));
    g_dns_server.proto.udp->local_port  = DNS_PORT;
    espconn_regist_recvcb(&g_dns_server, net_dns_recv);     /* 注册网络数据接收成功的回调函数，这里是处理dns请求的 */
    return espconn_create(&g_dns_server);
}

/* 注意，在SoftAP模式下可直接监听，在Station模式下需要等到获取地址再监听 */
sint8 ICACHE_FLASH_ATTR net_web_server_start(void)
{
    /* 注册web服务,tcp协议,端口为80 */
    g_web_server.type = ESPCONN_TCP;
    g_web_server.proto.tcp = (esp_tcp *) os_zalloc(sizeof(esp_tcp));
    g_web_server.proto.tcp->local_port = HTTP_PORT;
    espconn_regist_connectcb(&g_web_server, tcp_web_server_connected_cb);    /* 有设备连接成功需要回调的函数 */
    espconn_regist_reconcb(&g_web_server, tcp_reconn_cb);    
    espconn_regist_time(&g_web_server, 60, 0);                              //60s没收到数据，断开连接
    return espconn_accept(&g_web_server);
}

void ICACHE_FLASH_ATTR net_dns_server_stop(void)
{
    espconn_delete(&g_dns_server);
}

void ICACHE_FLASH_ATTR net_web_server_stop(void)
{
    espconn_delete(&g_web_server);
}

void ICACHE_FLASH_ATTR net_dns_server_restart(void)
{
    espconn_delete(&g_dns_server);
    os_delay_us(10000);
    net_dns_server_start();
}

void ICACHE_FLASH_ATTR net_web_server_restart(void)
{
    espconn_delete(&g_web_server);
    os_delay_us(10000);
    net_web_server_start();
}


//void ICACHE_FLASH_ATTR json_jump(struct jsonparse_state *parser)
//{    
//    int cnt = 0;
//    while(jsonparse_next(parser))
//    {
//        if(jsonparse_get_type(parser) == JSON_TYPE_PAIR)
//        {
//            if(++cnt >= 2)
//                break;
//        }
//        else
//            cnt = 0;
//    }
//}

void ICACHE_FLASH_ATTR net_save_data_judge(char *buffer, char * data, char * flag)
{
    if (os_strlen(buffer))
        os_sprintf(data, "【%s:%s】", flag, buffer);
    else
        data[0] = '\0';
    os_printf("%s:%s(%d)\n",flag, buffer, os_strlen(buffer));
}

int ICACHE_FLASH_ATTR json_location_parse( struct jsonparse_state *parser)
{
    int type;
    char buffer[256];
    while ((type = jsonparse_next(parser)) != 0){
        if (type == JSON_TYPE_PAIR_NAME){
            if (jsonparse_strcmp_value(parser, "weatherinfo") == 0){
                jsonparse_next(parser);
                jsonparse_next(parser);
                jsonparse_copy_value(parser, buffer, sizeof(buffer));
                net_save_data_judge(buffer, weather, "天气");
            }
            else if(jsonparse_strcmp_value(parser, "sentence") == 0){
                jsonparse_next(parser);
                jsonparse_next(parser);
                jsonparse_copy_value(parser, buffer, sizeof(buffer));
                net_save_data_judge(buffer, sentence, "美句");
            }
            else if(jsonparse_strcmp_value(parser, "cityid") == 0){
                jsonparse_next(parser);
                jsonparse_next(parser);
                jsonparse_copy_value(parser, buffer, sizeof(buffer));
                
                /* 如果返回的数据和本地不同则更新本地并保存 */
                if (os_strcmp(buffer, app_web_get_cityid()))
                {
                    os_strncpy(app_web_get_cityid(), buffer, os_strlen(app_web_get_cityid()) + 1);
                    app_web_data_save();
                }
            }
            else if(jsonparse_strcmp_value(parser, "news") == 0){
                jsonparse_next(parser);
                jsonparse_next(parser);
                jsonparse_copy_value(parser, buffer, sizeof(buffer));
                net_save_data_judge(buffer, news, "新闻");
            }
        }
    }
    return 0;
}

//成功接收到服务器返回数据函数
void ICACHE_FLASH_ATTR user_tcp_recv_cb(void *arg, char *pdata, unsigned short len) 
{
    char * json_head;
    struct jsonparse_state json_data;

    // pdata是收到的数据，len是收到数据的长度
    uart0_sendStr("\r\n---get web data start----- \r\n ");

    /* 服务器数据返回形式：
       {"weatherinfo":"南京今日多云,24℃~15℃,微风小于3级。明日多云,26℃~14℃",、
        "cityid":"101190101",
        "sentence":"但使龙城飞将在，不教胡马度阴山。--《出塞二首》",
        "news":"xxxxxx"}    后期可以在此继续扩展其他联网数据
    */
    if (os_strlen(pdata))
    {
        json_head = os_strstr(pdata, "{\"");
        if (json_head)
        {
            os_bzero(&json_data, sizeof(json_data));
            uart0_tx_buffer(json_head, os_strlen(json_head));
            jsonparse_setup(&json_data, json_head, os_strlen(json_head));
            json_location_parse(&json_data);
        }
    }
    uart0_sendStr("\r\n---get web data end----- \r\n ");
}

/* 更新实时信息 */
void ICACHE_FLASH_ATTR net_update_info(void)
{
    char http_buf[256];
    char buffer[] = "GET /%s?cityid=%s HTTP/1.1\r\n"
        "Host: %s:%d\r\n"
        "Connection: close\r\n\r\n";

    os_sprintf(http_buf, buffer, esp_get_id(), os_strlen(app_web_get_cityid())?app_web_get_cityid():"", SERVER_ADDR_STR, SERVER_PORT);
    os_printf("%s",http_buf);
    espconn_send(&g_tcp_client_conn, http_buf, os_strlen(http_buf));
}

void ICACHE_FLASH_ATTR user_tcp_connect_cb(void *arg) {
    struct espconn *pespconn = arg;
    espconn_regist_recvcb(pespconn, user_tcp_recv_cb);              // 注册收到数据时的处理函数
    espconn_regist_sentcb(pespconn, tcp_web_server_sent_cb);        // 数据发送成功之后的回调函数
    espconn_regist_disconcb(pespconn, tcp_web_server_disconn_cb);   // 注册成功断联之后的回调函数
    net_update_info();
}

void ICACHE_FLASH_ATTR net_client_init(unsigned int localip)
{
    char ret;
    struct ip_addr remote_ip = {SERVER_ADDR};
    struct ip_addr local_ip = {localip};
    
    os_printf("=>local "IPSTR" connect remote "IPSTR":%d\n", IP2STR(&local_ip.addr), IP2STR(&remote_ip.addr), SERVER_PORT);

    if (g_tcp_client_conn.proto.tcp != NULL)
    {
        espconn_delete(&g_tcp_client_conn);
        os_free(g_tcp_client_conn.proto.tcp);
    }

    os_bzero(&g_tcp_client_conn, sizeof(g_tcp_client_conn));
    g_tcp_client_conn.type = ESPCONN_TCP;                                  //连接类型
    g_tcp_client_conn.state = ESPCONN_NONE;
    g_tcp_client_conn.proto.tcp = (esp_tcp *) os_zalloc(sizeof(esp_tcp));
    os_memcpy(&g_tcp_client_conn.proto.tcp->local_ip, &local_ip, 4);        // 使用的本端连接地址
    os_memcpy(&g_tcp_client_conn.proto.tcp->remote_ip, &remote_ip, 4);      // 远端服务器地址
    g_tcp_client_conn.proto.tcp->local_port = espconn_port();               // 查找一个本端用于通信的端口
    g_tcp_client_conn.proto.tcp->remote_port = SERVER_PORT;                 // 远端访问的地址

    //注册连接成功之后的回调 和 连接失败之后重连回调
    espconn_regist_connectcb(&g_tcp_client_conn, user_tcp_connect_cb);
    espconn_regist_reconcb(&g_tcp_client_conn, tcp_reconn_cb);

    //开始连接服务器
    ret = espconn_connect(&g_tcp_client_conn);
    os_printf("=>0x%x connect status is %d\n", &g_tcp_client_conn, ret);
}
