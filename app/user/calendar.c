#include "osapi.h"

#include "calendar.h"

typedef struct _tagFestival
{
    unsigned char nong;
    unsigned char month;
    unsigned char day;
    const char * hz;
}T_Festival;


const char const *ChDay[] = {"*","初一","初二","初三","初四","初五",
                        "初六","初七","初八","初九","初十",
                        "十一","十二","十三","十四","十五",
                        "十六","十七","十八","十九","二十",
                        "廿一","廿二","廿三","廿四","廿五",
                        "廿六","廿七","廿八","廿九","三十"};
const char const *ChMonth[] = {"*","正","二","三","四","五","六","七","八","九","十","冬","腊"};


const char const *solarTerm[] = {"小寒", "大寒", "立春", "雨水", "惊蛰", "春分", "清明", "谷雨", "立夏", "小满", "芒种", "夏至", 
                            "小暑", "大暑", "立秋", "处暑", "白露", "秋分", "寒露", "霜降", "立冬", "小雪", "大雪", "冬至"};
T_Festival g_solarterm_list[24];
const T_Festival g_festival_list[] = {{0, 1, 1, "元旦"},
                                    {1, 12, 30, "除夕"},
                                    {1, 1, 1, "春节"},
                                    {1, 1, 15, "元宵节"},
                                    {0, 2, 14, "情人节"},
                                    {1, 2, 2, "龙抬头"},
                                    {0, 3, 8, "妇女节"},
                                    {0, 3, 12, "植树节"},
                                    {0, 4, 1, "愚人节"},
                                    {0, 5, 1, "劳动节"},
                                    {0, 5, 4, "青年节"},
                                    {0, 6, 1, "儿童节"},
                                    {1, 5, 5, "端午节"},
                                    {0, 7, 1, "建党节"},
                                    {0, 8, 1, "建军节"},
                                    {1, 7, 7, "七夕节"},
                                    {1, 7, 15,"中元节"},
                                    {0, 9, 10,"教师节"},
                                    {1, 8, 15,"中秋节"},
                                    {0, 10, 1,"国庆节"},
                                    {1, 9, 9, "重阳节"},
                                    {0, 11, 1, "万圣节"},
                                    {0, 12, 24, "平安夜"},
                                    {0, 12, 25, "圣诞节"}
                                    };


const unsigned int lunar200y[] = {
    0x04AE53,0x0A5748,0x5526BD,0x0D2650,0x0D9544,0x46AAB9,0x056A4D,0x09AD42,0x24AEB6,0x04AE4A,/*1901-1910*/
    0x6A4DBE,0x0A4D52,0x0D2546,0x5D52BA,0x0B544E,0x0D6A43,0x296D37,0x095B4B,0x749BC1,0x049754,/*1911-1920*/
    0x0A4B48,0x5B25BC,0x06A550,0x06D445,0x4ADAB8,0x02B64D,0x095742,0x2497B7,0x04974A,0x664B3E,/*1921-1930*/
    0x0D4A51,0x0EA546,0x56D4BA,0x05AD4E,0x02B644,0x393738,0x092E4B,0x7C96BF,0x0C9553,0x0D4A48,/*1931-1940*/
    0x6DA53B,0x0B554F,0x056A45,0x4AADB9,0x025D4D,0x092D42,0x2C95B6,0x0A954A,0x7B4ABD,0x06CA51,/*1941-1950*/
    0x0B5546,0x555ABB,0x04DA4E,0x0A5B43,0x352BB8,0x052B4C,0x8A953F,0x0E9552,0x06AA48,0x6AD53C,/*1951-1960*/
    0x0AB54F,0x04B645,0x4A5739,0x0A574D,0x052642,0x3E9335,0x0D9549,0x75AABE,0x056A51,0x096D46,/*1961-1970*/
    0x54AEBB,0x04AD4F,0x0A4D43,0x4D26B7,0x0D254B,0x8D52BF,0x0B5452,0x0B6A47,0x696D3C,0x095B50,/*1971-1980*/
    0x049B45,0x4A4BB9,0x0A4B4D,0xAB25C2,0x06A554,0x06D449,0x6ADA3D,0x0AB651,0x093746,0x5497BB,/*1981-1990*/
    0x04974F,0x064B44,0x36A537,0x0EA54A,0x86B2BF,0x05AC53,0x0AB647,0x5936BC,0x092E50,0x0C9645,/*1991-2000*/
    0x4D4AB8,0x0D4A4C,0x0DA541,0x25AAB6,0x056A49,0x7AADBD,0x025D52,0x092D47,0x5C95BA,0x0A954E,/*2001-2010*/
    0x0B4A43,0x4B5537,0x0AD54A,0x955ABF,0x04BA53,0x0A5B48,0x652BBC,0x052B50,0x0A9345,0x474AB9,/*2011-2020*/
    0x06AA4C,0x0AD541,0x24DAB6,0x04B64A,0x69573D,0x0A4E51,0x0D2646,0x5E933A,0x0D534D,0x05AA43,/*2021-2030*/
    0x36B537,0x096D4B,0xB4AEBF,0x04AD53,0x0A4D48,0x6D25BC,0x0D254F,0x0D5244,0x5DAA38,0x0B5A4C,/*2031-2040*/
    0x056D41,0x24ADB6,0x049B4A,0x7A4BBE,0x0A4B51,0x0AA546,0x5B52BA,0x06D24E,0x0ADA42,0x355B37,/*2041-2050*/
    0x09374B,0x8497C1,0x049753,0x064B48,0x66A53C,0x0EA54F,0x06B244,0x4AB638,0x0AAE4C,0x092E42,/*2051-2060*/
    0x3C9735,0x0C9649,0x7D4ABD,0x0D4A51,0x0DA545,0x55AABA,0x056A4E,0x0A6D43,0x452EB7,0x052D4B,/*2061-2070*/
    0x8A95BF,0x0A9553,0x0B4A47,0x6B553B,0x0AD54F,0x055A45,0x4A5D38,0x0A5B4C,0x052B42,0x3A93B6,/*2071-2080*/
    0x069349,0x7729BD,0x06AA51,0x0AD546,0x54DABA,0x04B64E,0x0A5743,0x452738,0x0D264A,0x8E933E,/*2081-2090*/
    0x0D5252,0x0DAA47,0x66B53B,0x056D4F,0x04AE45,0x4A4EB9,0x0A4D4C,0x0D1541,0x2D92B5          /*2091-2099*/
};



int monthTotal[13] = {0,31,59,90,120,151,181,212,243,273,304,334,365};

T_DateInfo ICACHE_FLASH_ATTR toSolar(T_DateInfo lunar){
    int year = lunar.year,
        month = lunar.month,
        day = lunar.day;
    int byNow, xMonth, i;
    T_DateInfo solar;
    
    byNow = (lunar200y[year-1901] & 0x001F) - 1;
    
    if( ((lunar200y[year-1901]&0x0060)>>5) == 2)
        byNow += 31;
    for(i = 1; i < month; i ++){
        if( ( lunar200y[year - 1901] & (0x80000 >> (i-1)) ) ==0){
            byNow += 29;
        }
        else
            byNow += 30;
    }
    byNow += day;
    xMonth = (lunar200y[year - 1901] & 0xf00000)>>20;
    if(xMonth != 0){
        if(month > xMonth
           ||(month==xMonth && lunar.reserved == 1)){
            if((lunar200y[year-1901] & (0x80000>>(month-1)))==0)
                byNow += 29;
            else
                byNow += 30;
        }
    }
    if(byNow > 366
       ||(year%4!=0 && byNow == 365)){
        year += 1;
        if(year%4==0)
            byNow -= 366;
        else
            byNow -= 365;
    }
    for(i=1; i <= 13; i ++){
        if(monthTotal[i] >= byNow){
            month = i;
            break;
        }
    }
    solar.day = byNow - monthTotal[month-1];
    solar.month = month;
    solar.year = year;
    
    return solar;
}

T_DateInfo ICACHE_FLASH_ATTR toLunar(T_DateInfo solar){
    int year = solar.year,
    month = solar.month,
    day = solar.day;
    int bySpring,bySolar,daysPerMonth;
    int index,flag;
    T_DateInfo lunar;
    
    //bySpring 记录春节离当年元旦的天数。
    //bySolar 记录阳历日离当年元旦的天数。
    if( ((lunar200y[year-1901] & 0x0060) >> 5) == 1)
        bySpring = (lunar200y[year-1901] & 0x001F) - 1;
    else
        bySpring = (lunar200y[year-1901] & 0x001F) - 1 + 31;
    bySolar = monthTotal[month-1] + day - 1;
    if( (!(year % 4)) && (month > 2))
        bySolar++;
    
    //daysPerMonth记录大小月的天数 29 或30
    //index 记录从哪个月开始来计算。
    //flag 是用来对闰月的特殊处理。
    
    //判断阳历日在春节前还是春节后
    if (bySolar >= bySpring) {//阳历日在春节后（含春节那天）
        bySolar -= bySpring;
        month = 1;
        index = 1;
        flag = 0;
        if( ( lunar200y[year - 1901] & (0x80000 >> (index-1)) ) ==0)
            daysPerMonth = 29;
        else
            daysPerMonth = 30;
        while(bySolar >= daysPerMonth) {
            bySolar -= daysPerMonth;
            index++;
            if(month == ((lunar200y[year - 1901] & 0xF00000) >> 20) ) {
                flag = ~flag;
                if(flag == 0)
                    month++;
            }
            else
                month++;
            if( ( lunar200y[year - 1901] & (0x80000 >> (index-1)) ) ==0)
                daysPerMonth=29;
            else
                daysPerMonth=30;
        }
        day = bySolar + 1;
    }
    else {//阳历日在春节前
        bySpring -= bySolar;
        year--;
        month = 12;
        if ( ((lunar200y[year - 1901] & 0xF00000) >> 20) == 0)
            index = 12;
        else
            index = 13;
        flag = 0;
        if( ( lunar200y[year - 1901] & (0x80000 >> (index-1)) ) ==0)
            daysPerMonth = 29;
        else
            daysPerMonth = 30;
        while(bySpring > daysPerMonth) {
            bySpring -= daysPerMonth;
            index--;
            if(flag == 0)
                month--;
            if(month == ((lunar200y[year - 1901] & 0xF00000) >> 20))
                flag = ~flag;
            if( ( lunar200y[year - 1901] & (0x80000 >> (index-1)) ) ==0)
                daysPerMonth = 29;
            else
                daysPerMonth = 30;
        }
        
        day = daysPerMonth - bySpring + 1;
    }
    lunar.day = day;
    lunar.month = month;
    lunar.year = year;
    if(month == ((lunar200y[year - 1901] & 0xF00000) >> 20))
        lunar.reserved = 1;
    else
        lunar.reserved = 0;
    return lunar;
}

/* 给定年月，返回指定月两个节气的日期，格式为xxxx,例如621，表示6号为某节气，21为某节气*/
int ICACHE_FLASH_ATTR CalSolarTerm(unsigned char year, unsigned char month)
{
    float c_value[] = { 5.4055f,     /* 1月小寒 */
                        20.12f,      /* 1月大寒 */
                        3.87f,      /* 2月立春 */
                        18.73f,     /* 2月雨水 */
                        5.63f,      /* 3月惊蛰 */
                        20.646f,    /* 3月春分 */
                        4.81f,      /* 4月清明 */
                        20.1f,      /* 4月谷雨 */
                        5.52f,      /* 5月立夏 */
                        21.04f,     /* 5月小满 */
                        5.678f,     /* 6月芒种 */
                        21.37f,     /* 6月夏至 */
                        7.108f,     /* 7月小暑 */
                        22.83f,     /* 7月大暑 */
                        7.5f,       /* 8月立秋 */
                        23.13f,     /* 8月处暑 */
                        7.646f,     /* 9月白露 */
                        23.042f,    /* 9月秋分 */
                        8.318f,     /* 10月寒露 */
                        23.438f,    /* 10月霜降 */
                        7.438f,     /* 11月立冬 */
                        22.36f,     /* 11月小雪 */
                        7.18f,      /* 12月大雪 */
                        21.94f,     /* 12月冬至 */
                        };
    int ret_day, ret_day2;

    // TODO:一些年份的节气误差未计算在内，可通过 https://www.jianshu.com/p/1f814c6bb475 查询误差
    if (year % 4)
    {
        ret_day = (int)(year * 0.2422 + c_value[(month - 1)* 2]) - ((year - 1) / 4);
        ret_day2 = (int)(year * 0.2422 + c_value[(month - 1)* 2 + 1]) - ((year - 1) / 4);
    }
    else
    {
        ret_day = (int)(year * 0.2422 + c_value[(month - 1)* 2]) - (year / 4);
        ret_day2 = (int)(year * 0.2422 + c_value[(month - 1)* 2 + 1]) - (year / 4);
        
    }
    return ret_day * 100 + ret_day2;
}

/* 只计算1年内的日期，year表示当前年 */
int ICACHE_FLASH_ATTR DateCountdown(unsigned char year, unsigned char month, unsigned char day,
                         unsigned char target_month, unsigned char target_day)
{
    int daysPerMonth[] = {0,31,28,31,30,31,30,31,31,30,31,30,31};
    int m, total_days = 0;

    if (target_month >= month)
    {
        for (m = month; m < target_month; m++)
        {
            if ((m == 2) && (year % 4 == 0)) total_days++;
            total_days += daysPerMonth[m];
        }
        return total_days - day + target_day;
    }

    /* 跨年 */
    for (m = 1; m < target_month; m++)
    {
        if ((m == 2) && ((year+1) % 4 == 0)) total_days++;
        total_days += daysPerMonth[m];
    }
    for (m = month; m <= 12; m++)
    {
        if ((m == 2) && ((year+1) % 4 == 0)) total_days++;
        total_days += daysPerMonth[m];
    }
    return total_days - day + target_day;
}

/* 初始化当年的节气日期 */
void ICACHE_FLASH_ATTR InitSolarTerm(unsigned char year)
{
    unsigned int ret_day;
    unsigned char  m, month;
    unsigned char day1, day2;
    
    for (m = 0; m < 12; m++)
    {
        month = m + 1;
        ret_day = CalSolarTerm(year, month);
        day2 = ret_day % 100;
        day1 = ret_day / 100;
        g_solarterm_list[m*2].nong = 0;
        g_solarterm_list[m*2].month = month;
        g_solarterm_list[m*2].day = day1;
        g_solarterm_list[m*2].hz = solarTerm[m*2];
        g_solarterm_list[m*2 + 1].nong = 0;
        g_solarterm_list[m*2 + 1].month = month;
        g_solarterm_list[m*2 + 1].day = day2;
        g_solarterm_list[m*2 + 1].hz = solarTerm[2*m + 1];
    }
}

/* 距离指定日期最近的一个节日剩多少天，并返回节日名 */
char * ICACHE_FLASH_ATTR FestivalCountdown(unsigned char year, unsigned char month, unsigned char day, int * nday)
{
    
    unsigned char m;
    int day_diff;
    char * ret_festival;
    *nday = 366;    /* 给个初始值 */
    
    
    for (m = 0; m < sizeof(g_festival_list) / sizeof(T_Festival); m++)
    {
        if (g_festival_list[m].nong == 0)   /* 阳历上的最短时间 */
        {
            day_diff = DateCountdown(year, month, day, g_festival_list[m].month, g_festival_list[m].day);
            
        }
        else
        {
            T_DateInfo calendar = {2000 + year, g_festival_list[m].month, g_festival_list[m].day, 0};
            calendar = toSolar(calendar);   /* 转阳历 */
            day_diff = DateCountdown(year, month, day, calendar.month, calendar.day);
        }
        
        if (day_diff >= 0 && day_diff < *nday)   /* 如果有比当前时间更短的节日时间则更换 */
        {
            *nday = day_diff;
            ret_festival = g_festival_list[m].hz;
        }
    }

    InitSolarTerm(year);
    for (m = 0; m < 24; m++)
    {
        day_diff = DateCountdown(year, month, day, g_solarterm_list[m].month, g_solarterm_list[m].day);

        if (day_diff >= 0 && day_diff < *nday)
        {
            *nday = day_diff;
            ret_festival = g_solarterm_list[m].hz;
        }
    }

    return ret_festival;
}

