#include "osapi.h"
#include "ip_addr.h"
#include "user_interface.h"
#include "sntp.h"

#include "app_net_server.h"
#include "app_sntp.h"
#include "app_web.h"
#include "calendar.h"

char g_festival_info[32] = {0};
const char* g_week_hz[] = {"一","二","三","四","五","六","日"};
static os_timer_t g_sntp_timer;
static T_TimeDetail g_time_info;
static char * g_time_str = NULL;
static bool ntp_need_update = true;

void ICACHE_FLASH_ATTR app_sntp_parse_date(char * date_str, uint8 * year, uint8 * month, uint8 * day)
{
    if (year)
        *year = (date_str[2] - '0') * 10 + (date_str[3] - '0');

    if (month)
        *month = (date_str[5] - '0') * 10 + (date_str[6] - '0');

    if (day)
        *day = (date_str[8] - '0') * 10 + (date_str[9] - '0');
}

void ICACHE_FLASH_ATTR app_sntp_parse_time(char * time_str, uint8 * hours, uint8 * minute, uint8 * second)
{
    if (hours)
        *hours = (time_str[0] - '0') * 10 + (time_str[1] - '0');

    if (minute)
        *minute = (time_str[3] - '0') * 10 + (time_str[4] - '0');

    if (second)
        *second = (time_str[6] - '0') * 10 + (time_str[7] - '0');
}

uint8* ICACHE_FLASH_ATTR app_sntp_get_weekstr(void)
{
    return g_week_hz[g_time_info.week-1];
}

int8* ICACHE_FLASH_ATTR app_sntp_get_festival(void)
{
    return g_festival_info;
}

uint8 ICACHE_FLASH_ATTR app_sntp_get_week(char * time_str)
{
    if (os_strstr(time_str, "Mon")) return 1;
    else if (os_strstr(time_str, "Tue")) return 2;
    else if (os_strstr(time_str, "Wed")) return 3;
    else if (os_strstr(time_str, "Thu")) return 4;
    else if (os_strstr(time_str, "Fri")) return 5;
    else if (os_strstr(time_str, "Sat")) return 6;
    else return 7;
}

int8* ICACHE_FLASH_ATTR app_sntp_get_month(char * time_str)
{
    if (os_strstr(time_str, "Jan")) return "01";
    else if (os_strstr(time_str, "Feb")) return "02";
    else if (os_strstr(time_str, "Mar")) return "03";
    else if (os_strstr(time_str, "Apr")) return "04";
    else if (os_strstr(time_str, "May")) return "05";
    else if (os_strstr(time_str, "Jun")) return "06";
    else if (os_strstr(time_str, "Jul")) return "07";
    else if (os_strstr(time_str, "Aug")) return "08";
    else if (os_strstr(time_str, "Sep")) return "09";
    else if (os_strstr(time_str, "Oct")) return "10";
    else if (os_strstr(time_str, "Nov")) return "11";
    else return "12";
}

uint8 ICACHE_FLASH_ATTR app_sntp_get_year(char * time_str)
{
    return (time_str[22] - '0') * 10 + (time_str[23] - '0');
}

uint8 ICACHE_FLASH_ATTR app_sntp_get_day(char * time_str)
{
    return (time_str[8] - '0') * 10 + (time_str[9] - '0');
}

uint8 ICACHE_FLASH_ATTR app_sntp_get_hour(char * time_str)
{
    return (time_str[11] - '0') * 10 + (time_str[12] - '0');
}

uint8 ICACHE_FLASH_ATTR app_sntp_get_minute(char * time_str)
{
    return (time_str[14] - '0') * 10 + (time_str[15] - '0');
}

uint8 ICACHE_FLASH_ATTR app_sntp_get_second(char * time_str)
{
    return (time_str[17] - '0') * 10 + (time_str[18] - '0');
}

void ICACHE_FLASH_ATTR app_sntp_get_time_from_str(void)
{
    static uint8 init_cnt = 0, f_hour = 0, f_min = 1;
    unsigned int current_stamp = sntp_get_current_timestamp() + 2;      /* 因为刷新频率不高，这里提前2秒校准 */
    char * current_time = sntp_get_real_time(current_stamp);
    T_DateInfo solar_calendar;

    //获取的时间戳
    if(current_stamp != 0 ){
        g_time_info.year = app_sntp_get_year(current_time);
        if (!wifi_get_station_ip_str() || g_time_info.year == 70) /* 获取的时间为1970年不显示 */
        {
            system_soft_wdt_feed();
            os_timer_disarm(&g_sntp_timer);
            ntp_need_update = true;
            init_cnt = 0;
            return;
        }
        
        g_time_info.month_str = app_sntp_get_month(current_time+3);
        g_time_info.month =  (g_time_info.month_str[0] - '0') * 10 + (g_time_info.month_str[1] - '0');
        os_strncpy(g_time_info.day_str, current_time + 8, 2);
        g_time_info.day = app_sntp_get_day(current_time);
        g_time_info.week = app_sntp_get_week(current_time);
        g_time_info.week_str = app_sntp_get_weekstr();
        g_time_info.hour = app_sntp_get_hour(current_time);
        g_time_info.minute = app_sntp_get_minute(current_time);
        g_time_info.second = app_sntp_get_second(current_time);
        solar_calendar.day = g_time_info.day;
        solar_calendar.month = g_time_info.month;
        solar_calendar.year = 2000 + g_time_info.year;
        solar_calendar = toLunar(solar_calendar);
        os_sprintf(g_time_info.lmonth_str, "%s%s月", solar_calendar.reserved?"闰":"", ChMonth[solar_calendar.month]);
        os_sprintf(g_time_info.lday_str, "%s", ChDay[solar_calendar.day]);
        g_time_str = current_time;

        /* 判断是否有闹钟 */
        app_web_clock_alarm_on(&g_time_info);

        /* 1~6小时(随机)更新一次天气，新闻等信息 */
        if ((f_hour == g_time_info.hour)
            && (f_min == g_time_info.minute)
            && (0 == g_time_info.second))
        {
            f_hour = (g_time_info.hour + os_random() % 6 + 1) % 24;
            f_min = (g_time_info.minute + os_random() % 60) % 60;
            net_client_init(wifi_get_station_ip());
        }

        /* 在零点必须做的事 */
        if (((0 == g_time_info.hour) && (0 == g_time_info.minute) && (1 == g_time_info.second)) 
            || !os_strlen(g_festival_info))
        {
            int cnt_day;
            char * fes = FestivalCountdown(g_time_info.year, g_time_info.month, g_time_info.day, &cnt_day);
            if (cnt_day) os_sprintf(g_festival_info, "【节日:距%s还有%d天】", fes, cnt_day);
            else os_sprintf(g_festival_info, "【节日:今日%s】", fes);
        }

//        os_printf("SNTP:%s\n%d-%d-%d %d:%d:%d %s %s%s\n", current_time,
//            g_time_info.year+2000, g_time_info.month, g_time_info.day, g_time_info.hour,
//            g_time_info.minute, g_time_info.second, g_time_info.week_str, g_time_info.lmonth_str, g_time_info.lday_str);
    }

    /* ntp更新频率 */
    if (++init_cnt >= NTP_INIT_TIME * 60)
    {
        ntp_need_update = true;
        init_cnt = 0;
    }

    system_soft_wdt_feed();
}

T_TimeDetail * app_sntp_get_time(void)
{
    return &g_time_info;
}

char * app_sntp_get_time_str(void)
{
    return g_time_str;
}

void ICACHE_FLASH_ATTR app_sntp_init(void)
{
    if (ntp_need_update && wifi_get_station_ip_str())
    {
        sntp_stop();
        sntp_setservername(0, NTP_SERVER1);    //设置SNTP主服务器，使用0表示
        sntp_setservername(1, NTP_SERVER2);    // 设置SNTP备服务器，使用1和2表示
        os_bzero(&g_time_info, sizeof(g_time_info));
        sntp_init();

        /* 重启定时获取时间 */
        os_timer_disarm(&g_sntp_timer);
        os_timer_setfn(&g_sntp_timer, (os_timer_func_t *)(app_sntp_get_time_from_str), NULL);
        os_timer_arm(&g_sntp_timer, NTP_TIMER_DELAY, true);
        ntp_need_update = false;
    }
}
