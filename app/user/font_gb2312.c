#include "osapi.h"
#include "user_interface.h"
#include "spi_flash.h"

#include "font_gb2312.h"


static unsigned int hz_gb2312_code[FONT_GB2312_SIZE/sizeof(uint32)] = {0};


unsigned char * ICACHE_FLASH_ATTR font_gb2312_get(char * hz)
{
    uint32 src_addr;
    SpiFlashOpResult ret;

    if (hz == NULL)
    {
        return NULL;
    }

    src_addr = ((hz[0] - 0xa1) * 94 + (hz[1] - 0xa1)) * FONT_GB2312_SIZE;
    ret = spi_flash_read(src_addr + FONT_GB2312_ADDR, (unsigned int * )hz_gb2312_code,  FONT_GB2312_SIZE);
    if (ret != SPI_FLASH_RESULT_OK)
    {
        return NULL;
    }

    return (unsigned char *)hz_gb2312_code;
}
