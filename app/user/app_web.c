#include "ets_sys.h"
#include "osapi.h"
#include "mem.h"
#include "ip_addr.h"

#include "espconn.h"
#include "user_interface.h"

#include "driver_oled12864.h"
#include "app_flash.h"
#include "app_sntp.h"
#include "app_web.h"
#include "app_wifi.h"



typedef struct web_data{
    T_PER_CLOCK s_per_clock[PER_CLOCK_NUM];     /* 单次/每日闹钟数据 */
    T_CIR_CLOCK s_cir_clock[CIR_CLOCK_NUM];     /* 每周循环闹钟数据  */
    char s_city_id[CITYP_ID_LEN];
    char s_web_notes[WEB_NOTE_LEN];
    char s_enable[EN_MAX];
}T_WEB_DATA;

T_WEB_DATA g_data;      /* 需要保存的数据 */
T_PER_CLOCK * const g_per_clock = g_data.s_per_clock;
T_CIR_CLOCK * const g_cir_clock = g_data.s_cir_clock;
char * const g_city_id = g_data.s_city_id;
char * const g_web_notes = g_data.s_web_notes;

static const char *http_head_200 =   "HTTP/1.1 200 OK\r\n"
                                "Content-Type: text/html\r\n"
                                "Connection: close\r\n"
                                "Content-Length: %d \r\n\r\n";

const char page_success[] = "\
<!DOCTYPE html>\
<html lang='ch'>\
<head>\
<meta charset='gb2312'>\
<title>迷你时钟</title>\
</head>\
<body bgcolor='#f2e8d5' style='text-align:center'>\
<h1>迷你时钟控制台</h1>\
%s<br>\
<input type='button' value='返回' onclick='history.back()'>\
</body>\
</html>";

const char page_head_html[] = "\
<!DOCTYPE html>\
<html lang='ch'>\
<head>\
<meta charset='gb2312'>\
<title>迷你时钟</title>\
</head>\
<body bgcolor='#f2e8d5' style='text-align:center'>\
<h1>迷你时钟控制台</h1>\
<hr>\
<form name='input' action='/' method='POST'>\
<input type='text' name='id' value='MinClock-12341-cfg-wifi' hidden><br> \
<label>wifi 名字: </label>\
<input type='text' name='ssid'><br>\
<label>wifi 密码: </label>\
<input type='password' name='password'><br>\
<input type='submit' value='配 置 WIFI' style='width:100px;'>\
</form>\
<hr>";


const char page_end_html[] = "\
</body>\
</html>";

const char page_form_head_html[] = "\
<form name='input' action='/' method='POST' enctype='text/plain'>\
<input type='text' name='id' value='%s' hidden>";

const char page_note_html[]="\
<label>备忘消息: </label><input type='text' name='notes' value='%s' maxlength=128 >";

const char page_cityid_html[]="\
<a href='http://t.csdn.cn/5R1wF'>城市 ID: </a>&nbsp;&nbsp;\
<input type='number' name='city_id' value='%s' maxlength='9' oninput=\"value=value.replace(/[^\\d]/g,'')\">";

const char page_perclock_html[] = "\
<label>单次/每日闹钟 - %d : </label>\
<input type='date' name='clock%d-date' value='%s'>&nbsp;\
<input type='time' name='clock%d-time' value='%s'>\
<input type='checkbox' name='clock%d-enable' %s><br>"; // %s 添加checked即表示打钩

const char page_circlock_html[] ="\
<label>每周循环闹钟 - %d：</label><input type='time' name='cir-clock-time%d' value='%s'><br>\
<input type='checkbox' name='cbox-clock%d-Mon' %s>一\
<input type='checkbox' name='cbox-clock%d-Tue' %s>二\
<input type='checkbox' name='cbox-clock%d-Wed' %s>三\
<input type='checkbox' name='cbox-clock%d-Thu' %s>四\
<input type='checkbox' name='cbox-clock%d-Fri' %s>五\
<input type='checkbox' name='cbox-clock%d-Sat' %s>六\
<input type='checkbox' name='cbox-clock%d-Sun' %s>日\
<br>";

const char page_enable_html[] = "\
<input type='checkbox' name='%s' checked><label>%s</label><br>";

const char page_form_end_html[] = "\
<br><input type='submit' value='设置' style='width:100px;'>\
</form><hr>";

bool ICACHE_FLASH_ATTR app_web_get_data_enable(E_ENABLE_CFG cfg)
{
    return (bool)g_data.s_enable[cfg];
}


void ICACHE_FLASH_ATTR app_web_data_save(void)
{
    app_flash_write(&g_data, FLASH_HEAD_ADDRESS, sizeof(g_data));
}

char * ICACHE_FLASH_ATTR app_web_get_cityid(void)
{
    return g_data.s_city_id;
}

char * ICACHE_FLASH_ATTR app_web_get_note(bool is_src_str)
{
    static char notes[WEB_NOTE_LEN + 20];
    if (g_web_notes[0])
    {
        if (is_src_str)
            return g_web_notes;
        else
        {
            os_sprintf(notes, "【备忘:%s】", g_web_notes);
            return notes;
        }
    }
    
    return NULL;
}

/* 构造控制页面 */
void ICACHE_FLASH_ATTR app_web_cfg_inf(struct espconn * espconn_fd)
{
    uint8 i;
    char * http_buffer = os_zalloc(4500);      /* 注意需要较大的栈空间需要使用alloc申请 */
    char * http_body_buffer = os_zalloc(4096);

    /* html头设置 */
    system_soft_wdt_feed();
    os_strcpy(http_body_buffer, page_head_html);

    /* 备忘设置 */
    system_soft_wdt_feed();
    os_sprintf(http_buffer, page_form_head_html, "MinClock-12341-cfg-note");
    os_strcat(http_body_buffer, http_buffer);
    os_sprintf(http_buffer, page_note_html, g_web_notes);
    os_strcat(http_body_buffer, http_buffer);
    os_strcat(http_body_buffer, page_form_end_html);

    /* 城市id设置 */
    system_soft_wdt_feed();
    os_sprintf(http_buffer, page_form_head_html, "MinClock-12341-cfg-city");
    os_strcat(http_body_buffer, http_buffer);
    os_sprintf(http_buffer, page_cityid_html, g_city_id);
    os_strcat(http_body_buffer, http_buffer);
    os_strcat(http_body_buffer, page_form_end_html);

    /* 单次/每日闹钟 */
    system_soft_wdt_feed();
    os_sprintf(http_buffer, page_form_head_html, "MinClock-12341-cfg-perclock");
    os_strcat(http_body_buffer, http_buffer);
    for (i = 0; i < PER_CLOCK_NUM; i++)
    {
        int8 time[6];
        int8 date[12];
        system_soft_wdt_feed();
        os_sprintf(time, "%02d:%02d", g_per_clock[i].hour, g_per_clock[i].minute);
        os_sprintf(date, "20%02d-%02d-%02d", g_per_clock[i].year, g_per_clock[i].month, g_per_clock[i].day);
        os_sprintf(http_buffer, page_perclock_html, i+1, i+1, 
                                (g_per_clock[i].year+g_per_clock[i].month+g_per_clock[i].day) ? date : "",
                                i+1, time, i+1, g_per_clock[i].enable ? "checked"  :"");
        os_strcat(http_body_buffer, http_buffer);
    }
    os_strcat(http_body_buffer, page_form_end_html);

    /* 每周循环闹钟 */
    system_soft_wdt_feed();
    os_sprintf(http_buffer, page_form_head_html, "MinClock-12341-cfg-circlock");
    os_strcat(http_body_buffer, http_buffer);
    for (i = 0; i < CIR_CLOCK_NUM; i++)
    {
        uint8 time[8];
        system_soft_wdt_feed();
        os_sprintf(time, "%02d:%02d", g_cir_clock[i].hour, g_cir_clock[i].minute);
        os_sprintf(http_buffer, page_circlock_html, i+1, i+1, time,
                                                    i+1,g_cir_clock[i].week_enable[0]?"checked":"",
                                                    i+1,g_cir_clock[i].week_enable[1]?"checked":"",
                                                    i+1,g_cir_clock[i].week_enable[2]?"checked":"",
                                                    i+1,g_cir_clock[i].week_enable[3]?"checked":"",
                                                    i+1,g_cir_clock[i].week_enable[4]?"checked":"",
                                                    i+1,g_cir_clock[i].week_enable[5]?"checked":"",
                                                    i+1,g_cir_clock[i].week_enable[6]?"checked":"");
        os_strcat(http_body_buffer, http_buffer);
    }
    system_soft_wdt_feed();
    os_strcat(http_body_buffer, page_form_end_html);

    /* 配置使能 */
    system_soft_wdt_feed();
    os_sprintf(http_buffer, page_form_head_html, "MinClock-12341-cfg-enable");
    os_strcat(http_body_buffer, http_buffer);
    os_sprintf(http_buffer, page_enable_html, "clock-note", " 闹钟关联备忘");
    os_strcat(http_body_buffer, http_buffer);
    os_sprintf(http_buffer, page_enable_html, "enable-weather", " 天气信息显示");
    os_strcat(http_body_buffer, http_buffer);
    os_sprintf(http_buffer, page_enable_html, "enable-new", " 新闻信息显示");
    os_strcat(http_body_buffer, http_buffer);
    os_sprintf(http_buffer, page_enable_html, "enable-sentens", " 诗词美句显示");
    os_strcat(http_body_buffer, http_buffer);
    os_strcat(http_body_buffer, page_form_end_html);
    
    os_strcat(http_body_buffer, page_end_html);

    /* html尾部设置 */
    system_soft_wdt_feed();
    os_sprintf(http_buffer, http_head_200, os_strlen(http_body_buffer));    //页面长度
    os_strcat(http_buffer, http_body_buffer);
    espconn_send(espconn_fd, http_buffer, os_strlen(http_buffer));

    os_printf("http_buffer = %d, http_body_buffer = %d\n", strlen(http_buffer), strlen(http_body_buffer));
    os_free(http_buffer);
    os_free(http_body_buffer);
}

void ICACHE_FLASH_ATTR app_web_clock_alarm_on(T_TimeDetail * cur_time)
{
    uint8 i;

    if (cur_time && cur_time->second > 1)
    {
        return;
    }

    for (i = 0; i < PER_CLOCK_NUM; i++)
    {
        /* 当闹钟使能，alarm属于非警告状态，当前时间秒小于等于1秒才会设置闹钟 */
        if (g_per_clock[i].enable && !g_per_clock[i].alarm) 
        {
            if ((g_per_clock[i].day == 0
                && g_per_clock[i].hour == cur_time->hour
                && g_per_clock[i].minute == cur_time->minute)
            || (g_per_clock[i].year == cur_time->year
                && g_per_clock[i].month == cur_time->month
                && g_per_clock[i].day == cur_time->day
                && g_per_clock[i].hour == cur_time->hour
                && g_per_clock[i].minute == cur_time->minute))
            {
                g_per_clock[i].alarm = true;
            }
        }
    }

    for (i = 0; i < CIR_CLOCK_NUM; i++)
    {
        if (g_cir_clock[i].enable 
        && !g_cir_clock[i].alarm 
        && g_cir_clock[i].week_enable[cur_time->week-1]
        && g_cir_clock[i].hour == cur_time->hour 
        && g_cir_clock[i].minute == cur_time->minute)
        {
            g_cir_clock[i].alarm = true;
        }
    }
}

bool ICACHE_FLASH_ATTR app_web_clock_alarm_status(void)
{
    uint8 i;
    for (i = 0; i < PER_CLOCK_NUM; i++)
    {
        if(g_per_clock[i].alarm) return true;
    }

    for (i = 0; i < CIR_CLOCK_NUM; i++)
    {
        if(g_cir_clock[i].alarm) return true;
    }
    return false;
}

void ICACHE_FLASH_ATTR app_web_clock_alarm_off(void)
{
    uint8 i;
    for (i = 0; i < PER_CLOCK_NUM; i++)
    {
        g_per_clock[i].alarm = false;
    }

    for (i = 0; i < CIR_CLOCK_NUM; i++)
    {
        g_cir_clock[i].alarm = false;
    }
}


bool ICACHE_FLASH_ATTR app_web_clock_enable_status(void)
{
    uint8 i;
    for (i = 0; i < PER_CLOCK_NUM; i++)
    {
        if(g_per_clock[i].enable) return true;
    }

    for (i = 0; i < CIR_CLOCK_NUM; i++)
    {
        if(g_cir_clock[i].enable) return true;
    }
    return false;
}

void ICACHE_FLASH_ATTR app_web_ret_interface(struct espconn * espconn_fd, const char * show_info)
{
    char buffer[2048];
    char body[1024];
    
    os_sprintf(body, page_success, show_info);
    os_sprintf(buffer, http_head_200, os_strlen(body));
    os_strcat(buffer, body);
    espconn_send(espconn_fd, buffer,os_strlen(buffer));
}

/* 解析出wifi的密码账号信息 */
void ICACHE_FLASH_ATTR copy_wifi_info(char * s1, char * str, char * save_s)
{
    char * wifi_info;
    wifi_info = os_strstr(s1, str);

    if (wifi_info != NULL)
    {
        wifi_info += os_strlen(str);
        while (*wifi_info != '\0' && *wifi_info != '&')
        {
            *save_s++ = *wifi_info++;
        }
        *save_s = '\0';
    }
}

void ICACHE_FLASH_ATTR copy_strings_info(char * s1, char * str, char * save_s)
{
    char * note_info;
    note_info = os_strstr(s1, str);

    if (note_info != NULL)
    {
        note_info += os_strlen(str);
        while (*note_info != 0x0a && *note_info != 0x0d)
        {
            *save_s++ = *note_info++;
        }
        *save_s = '\0';
    }
}

bool ICACHE_FLASH_ATTR app_web_parse_alm_week(char * http_data, unsigned char * week, char i)
{
    char buffer[20];
    
    os_sprintf(buffer, "cbox-clock%d-Mon", i);
    week[0] = os_strstr(http_data, buffer)?1:0;
    os_sprintf(buffer, "cbox-clock%d-Tue", i);
    week[1] = os_strstr(http_data, buffer)?1:0;
    os_sprintf(buffer, "cbox-clock%d-Wed", i);
    week[2] = os_strstr(http_data, buffer)?1:0;
    os_sprintf(buffer, "cbox-clock%d-Thu", i);
    week[3] = os_strstr(http_data, buffer)?1:0;
    os_sprintf(buffer, "cbox-clock%d-Fri", i);
    week[4] = os_strstr(http_data, buffer)?1:0;
    os_sprintf(buffer, "cbox-clock%d-Sat", i);
    week[5] = os_strstr(http_data, buffer)?1:0;
    os_sprintf(buffer, "cbox-clock%d-Sun", i);
    week[6] = os_strstr(http_data, buffer)?1:0;

    for (i = 0; i < 7; i++)
    {
        if (week[i]) return true;
    }
    return false;
}

//解析后的数据都要保存到flash中
void ICACHE_FLASH_ATTR app_web_parse_http(struct espconn * espconn_fd, char * http_data)
{
    uint8 i;
    char out_ret[32] = {0};
    char out_ret2[32] = {0};
    char err_info[128] = "设置成功!!";
    char buffer[4096]={0};
    
    if (os_strstr(http_data, "cfg-wifi"))
    {
        copy_wifi_info(http_data, "ssid=", out_ret);
        copy_wifi_info(http_data, "password=", out_ret2);
        if (os_strlen(out_ret2) > 0 && os_strlen(out_ret) > 0)
        {
            app_web_ret_interface(espconn_fd, err_info);
            wifi_station_init(out_ret, out_ret2);
            return;
        }
    }
    else if (os_strstr(http_data, "cfg-enable"))
    {
        g_data.s_enable[EN_CLOCK_NOTES] = os_strstr(http_data, "clock-note")?1:0;
        g_data.s_enable[EN_WEATHER] = os_strstr(http_data, "enable-weather")?1:0;
        g_data.s_enable[EN_NEWS] = os_strstr(http_data, "enable-new")?1:0;
        g_data.s_enable[EN_SENTENCE] = os_strstr(http_data, "enable-sentens")?1:0;
        app_web_data_save();
    }
    else if (os_strstr(http_data, "cfg-note"))
    {
        copy_strings_info(http_data, "notes=", out_ret);
        if(os_strlen(out_ret))
            os_sprintf(g_web_notes, "%s", out_ret);
        else
            g_web_notes[0] = '\0';
        app_web_data_save();
    }
    else if (os_strstr(http_data, "cfg-city"))
    {
        /* 城市id需要保存到flash中 */
        
        copy_strings_info(http_data, "city_id=", out_ret);
        if (os_memcmp(out_ret, g_city_id, CITYP_ID_LEN-1))
        {
            os_strcpy(g_city_id, out_ret);
            app_web_data_save();
            oled12864_i2c_16x16_hzstr(0, 0, "即将重启更新天气", 0);
            system_restart();
            while(1)os_delay_us(10000);
        }
    }
    else if (os_strstr(http_data, "cfg-perclock"))  /* 配置单次闹钟 */
    {
        for (i = 0; i < CIR_CLOCK_NUM; i++)
        {
            os_sprintf(buffer, "clock%d-enable=on", i+1);
            g_per_clock[i].enable = os_strstr(http_data, buffer) ? 1 : 0;
            
            os_sprintf(buffer, "clock%d-date=", i+1);
            copy_strings_info(http_data, buffer, out_ret);
            if (out_ret[0])
                app_sntp_parse_date(out_ret, &g_per_clock[i].year, &g_per_clock[i].month, &g_per_clock[i].day);
            else
            {
                g_per_clock[i].year = 0;
                g_per_clock[i].month = 0;
                g_per_clock[i].day = 0;
            }
            
            os_sprintf(buffer, "clock%d-time=", i+1);
            copy_strings_info(http_data, buffer, out_ret);
            if (out_ret[0])
                app_sntp_parse_time(out_ret, &g_per_clock[i].hour, &g_per_clock[i].minute, NULL);
            else
            {
                g_per_clock[i].hour = 0;
                g_per_clock[i].minute = 0;
                if (g_per_clock[i].enable)
                {
                    os_strcpy(err_info, "设置失败: 请设置闹钟时间!");
                    g_per_clock[i].enable = 0;
                }
            }
            app_web_data_save();
            os_printf("%d:20%d-%d-%d %d:%d %s\n",i+1, g_per_clock[i].year, g_per_clock[i].month, g_per_clock[i].day,
                                                    g_per_clock[i].hour, g_per_clock[i].minute, g_per_clock[i].enable?"yes":"no");
        }
    }    
    else if (os_strstr(http_data, "cfg-circlock"))  /* 配置每周循环闹钟 */
    {
        for (i = 0; i < PER_CLOCK_NUM; i++)
        {
            g_cir_clock[i].enable = app_web_parse_alm_week(http_data, g_cir_clock[i].week_enable,i+1) ? 1 : 0;
            os_sprintf(buffer, "cir-clock-time%d=", i+1);
            copy_strings_info(http_data, buffer, out_ret);
            if (out_ret[0])
                app_sntp_parse_time(out_ret, &g_cir_clock[i].hour, &g_cir_clock[i].minute, NULL);
            else
            {
                g_cir_clock[i].hour = 0;
                g_cir_clock[i].minute = 0;
                if (g_cir_clock[i].enable)
                {
                    os_strcpy(err_info, "设置失败: 请设置闹钟时间!");
                    g_cir_clock[i].enable = 0;
                }
            }
            os_printf("=>cfg-circlock%d is %s enable (%d:%d)\n",i, g_cir_clock[i].enable?"":"not", g_cir_clock[i].hour,g_cir_clock[i].minute);
            app_web_data_save();
        }
    }

    /* 返回一个设置成功页面 */
    app_web_ret_interface(espconn_fd, err_info);


    return;
}

void ICACHE_FLASH_ATTR app_web_init(void)
{
    app_flash_read(&g_data, FLASH_HEAD_ADDRESS, sizeof(g_data));
    if (g_city_id[0] == 0x0ff)  /* 从flash读出的数据为0xff */
    {
        os_bzero(&g_data, sizeof(g_data));
        os_memset(g_data.s_enable, true, sizeof(g_data.s_enable));
        os_memcpy(g_city_id, "101190101", CITYP_ID_LEN);    /* 默认给南京的信息 */
        app_web_data_save();
    }
}
