#include "osapi.h"
#include "user_interface.h"
#include "gpio.h"
#include "driver/i2c_master.h"

#include "driver_SHT3X.h"


#define POLYNOMIAL  0x31 // P(x) = x^8 + x^5 + x^4 + 1 = 00110001

//==============================================================================
unsigned char ICACHE_FLASH_ATTR SHT3X_CalcCrc(unsigned char *crcdata, unsigned char nbrOfBytes){

    unsigned char Bit;        // bit mask
    unsigned char crc = 0xFF; // calculated checksum
    unsigned char byteCtr;    // byte counter

    // calculates 8-Bit checksum with given polynomial 
    for(byteCtr = 0; byteCtr < nbrOfBytes; byteCtr++)
    {
        crc ^= (crcdata[byteCtr]);
        for(Bit = 8; Bit > 0; --Bit)
        {
            if(crc & 0x80) crc = (crc << 1) ^ POLYNOMIAL;
            else           crc = (crc << 1);
        }
    }
    return crc;
}

/* 写单个字节 */
etError ICACHE_FLASH_ATTR sht3x_writebyte(unsigned char WRByte)
{
    i2c_master_writeByte(WRByte);
    if(i2c_master_checkAck())  return NO_ERROR;
    return ACK_ERROR;
}

/* 读取一个字节，返回指定的ack信号，ack/nack */
unsigned char ICACHE_FLASH_ATTR sht3x_readbyte(unsigned char ack_value)
{
    unsigned char RDByte = i2c_master_readByte();
    i2c_master_setAck(ack_value);
    return RDByte;
}

etError ICACHE_FLASH_ATTR sht3x_write_command(etCommands cmd)
{
  etError error;
  error  = sht3x_writebyte(cmd >> 8);
  error |= sht3x_writebyte(cmd & 0xFF);
  return error;
}

void ICACHE_FLASH_ATTR sht3x_init(void)
{
    i2c_master_start();
    i2c_master_writeByte(SHT3XWriteHeader);
    i2c_master_checkAck();
    sht3x_write_command(CMD_MEAS_PERI_1_H);
//    i2c_master_writeByte(CMD_MEAS_PERI_1_H >> 8);
//    i2c_master_checkAck();
//    i2c_master_writeByte(CMD_MEAS_PERI_1_H & 0xff);
//    i2c_master_checkAck();
    i2c_master_stop();
}

etError sht3x_get_temp_humi(int *temp, int*humi)
{

    etError error;           // error code
    unsigned long int    rawValueTemp;    // temperature raw value from sensor
    unsigned long int    rawValueHumi;    // humidity raw value from sensor
    unsigned char Rdata[6]={0};
    unsigned char i;

    i2c_master_start();
    error  = sht3x_writebyte(SHT3XWriteHeader);

    if(error == NO_ERROR)
    {
        // start measurement in polling mode
        // use depending on the required repeatability, the corresponding command
        error = sht3x_write_command(CMD_FETCH_DATA);
    }
    // if no error, wait until measurement ready
    if(error == NO_ERROR)
    {
        i2c_master_start();
        error = sht3x_writebyte(SHT3XReadHeader);
        // if measurement has finished -> exit loop
    }
    // if no error, read temperature and humidity raw values
    if(error == NO_ERROR)
    {
        for(i=0;i<5;i++)
        {
        Rdata[i] = sht3x_readbyte(ACK);
        }
        Rdata[i] = sht3x_readbyte(NACK);
        i2c_master_stop();
        if(Rdata[2]!=SHT3X_CalcCrc(Rdata,2))     error = CHECKSUM_ERROR;
        if(Rdata[5]!=SHT3X_CalcCrc(&Rdata[3],2)) error = CHECKSUM_ERROR;	
    }
    else
    {
        i2c_master_stop();
        return error;
    }
    // if no error, calculate temperature in  and humidity in %RH
    if(error == NO_ERROR)
    {
        rawValueTemp =(Rdata[0] << 8) | Rdata[1];
        rawValueHumi =(Rdata[3] << 8) | Rdata[4];
        *temp =(int)(1750 *rawValueTemp / 65535 - 450);        //  温度真实值需要x10,例如返回值是123,那么真是的温度就是12.3℃
        *humi =(int)(1000 *rawValueHumi / 65535);              //  同上
    }
    return error;
}

